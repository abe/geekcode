/*
 gc_computers.c - Functions for the Computers section

 Geek Code Generator v1.7.3 - Generates your geek code
 Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdio.h>
#include "geekcode.h"

int get_computers(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Computers                                                          Page %i of %i\n", PAGES-(PAGES-6), PAGES);
	printf("===============================================================================\n");
        printf("1 C++++ I'll be the first in line to get the new cybernetic interface installed\n");
        printf("        into my skull.\n");
        printf("2 C+++  You mean there is life outside of Internet? You're shittin' me!\n");
        printf("        I haven't dragged myself to class in weeks.\n");
        printf("3 C++   Computers are a large part of my existence. When I get up in the\n");
        printf("        morning the first thing I do is log myself in. I play games or mud on\n");
        printf("        weekends, but still manage to stay off academic probation.\n");
        printf("4 C+    Computers are fun and I enjoy using them. I play a mean game of Doom\n");
        printf("        and can use a word processor without resorting to the manual too often.\n");
        printf("        I know that a 3.5\" disk is not a hard disk. I also know that when it\n");
        printf("        says 'press any key to continue', I don't have to look for a key\n");
        printf("        labeled ANY\n");
        printf("5 C     Computers are a tool, nothing more. I use it when it serves my purpose.\n");
        printf("6 C-    Anything more complicated than my calculator and I'm screwed.\n");
        printf("7 C--   Where's the ON switch?\n");
        printf("8 C---  If you even mention computers, I will rip your head off!\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("Enter your Computers code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 8);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_unix_type(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("UNIX Type                                                         Page %ia of %i\n", PAGES-(PAGES-7), PAGES);
	printf("===============================================================================\n");
	printf(" 1 B - BSD (use unless your BSDish system is mentioned below)\n");
	printf(" 2 L - GNU/Linux\n");
	printf(" 3 U - Ultrix\n");
	printf(" 4 A - AIX\n");
	printf(" 5 V - SysV\n");
	printf(" 6 H - HPUX\n");
	printf(" 7 I - IRIX\n");
	printf(" 8 O - OSF/1 (aka Digital Unix)\n");
	printf(" 9 S - Sun OS/Solaris\n");
	printf("10 C - SCO Unix\n");
	printf("11 X - NeXT\n");
	printf("12 * - Some other one not listed\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("Enter your UNIX Type code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 12);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_unix(int x)
{
   /* Auto UNIX Type display added in v1.2 */
   int selection = 99;
   char *unix_type;

   switch (x)
   {
    case  1: unix_type="B"; break;
    case  2: unix_type="L"; break;
    case  3: unix_type="U"; break;
    case  4: unix_type="A"; break;
    case  5: unix_type="V"; break;
    case  6: unix_type="H"; break;
    case  7: unix_type="I"; break;
    case  8: unix_type="O"; break;
    case  9: unix_type="S"; break;
    case 10: unix_type="C"; break;
    case 11: unix_type="X"; break;
    case 12: unix_type="*"; break;
    default: unix_type="";  break;
   }

   do
     {
	clearscreen();
	printf("UNIX                                                              Page %ib of %i\n", PAGES-(PAGES-7), PAGES);
	printf("===============================================================================\n");
        printf("1 U%s++++ I am the sysadmin. If you try and crack my machine don't be surprised\n", unix_type);
        printf("         if the municipal works department gets an \"accidental\" computer\n");
        printf("         generated order to start a new landfill on your front lawn or your\n");
        printf("         quota is reduced to 4Kb.\n");
        printf("2 U%s+++  I don't need to crack /etc/passwd because I just modified su so that\n", unix_type);
        printf("         it doesn't prompt me. The admin staff doesn't even know I'm here. If\n");
        printf("         you don't understand what I just said, this category does NOT apply\n");
        printf("         to you!\n");
        printf("3 U%s++   I've got the entire admin ticked at me because I am always using all\n", unix_type);
        printf("         of the CPU time and trying to run programs that I don't have access\n");
        printf("         to. I'm going to try cracking /etc/passwd next week, just don't tell\n");
        printf("         anyone.\n");
        printf("4 U%s+    I not only have a Unix account, but slam VMS any chance I get.\n", unix_type);
        printf("5 U%s     I have a Unix account to do my stuff in.\n", unix_type);
        printf("6 U%s-    I have a VMS account.\n", unix_type);
        printf("7 U%s--   I've seen Unix and didn't like it. DEC rules!\n", unix_type);
        printf("8 U%s---  Unix geeks are actually nerds in disguise.\n", unix_type);
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("Enter your UNIX code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 8);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_perl(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Perl                                                               Page %i of %i\n", PAGES-(PAGES-8), PAGES);
	printf("===============================================================================\n");
        printf(" 1 P+++++ I am Larry Wall, Tom Christiansen, or Randal Schwartz.\n");
        printf(" 2 P++++  I don't write Perl, I speak it. Perl has superseded all other\n");
        printf("          programming languages. I firmly believe that all programs can be\n");
        printf("          reduced to a Perl one-liner. I use Perl to achieve U+++ status.\n");
        printf(" 3 P+++   Perl is a very powerful programming tool. Not only do I no longer\n");
        printf("          write shell scripts, I also no longer use awk or sed. I use Perl for\n");
        printf("          all programs of less than a thousand lines.\n");
        printf(" 4 P++    Perl is a powerful programming tool. I don't write shell scripts\n");
        printf("          anymore because I write them in Perl.\n");
        printf(" 5 P+     I know of Perl. I like Perl. I just haven't learned much Perl, but it\n");
        printf("          is on my agenda.\n");
        printf(" 6 P      I know Perl exists, but that's all.\n");
        printf(" 7 P-     What's Perl got that awk and sed don't have?\n");
        printf(" 8 P--    Perl users are sick, twisted programmers who are just showing off.\n");
        printf(" 9 P---   Perl combines the power of sh, the clarity of sed, and the\n");
        printf("          performance of awk, with the simplicity of C. It should be banned.\n");
        printf("10 P!     Our paranoid admin won't let us install Perl!\n");
        printf("          Says it's a 'hacking tool'.\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("Enter your Perl code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 10);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_linux(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("GNU/Linux                                                          Page %i of %i\n", PAGES-(PAGES-9), PAGES);
	printf("===============================================================================\n");
        printf("1 L+++++ I am Linus, grovel before me.\n");
        printf("2 L++++  I am a Linux wizard. I munch C code for breakfast and have enough room\n");
        printf("         left over for kernel debugging. I have so many patches installed that\n");
        printf("         I lost track about ten versions ago. Linux newbies consider me a god\n");
        printf("3 L+++   I use Linux exclusively on my system. I monitor comp.os.linux.* and\n");
        printf("         even answer questions sometimes.\n");
        printf("4 L++    I use Linux ALMOST exclusively on my system. I've given up trying to\n");
        printf("         achieve Linux god status, but welcome the OS as a replacement for DOS\n");
        printf("         I only boot to DOS to play games.\n");
        printf("5 L+     I've managed to get Linux installed and even used it a few times. It\n");
        printf("         seems like it is just another OS.\n");
        printf("6 L      I know what Linux is, but that's about all.\n");
        printf("7 L-     I have no desire to use Linux and frankly don't give a rat's patootie\n");
        printf("         about it. There are other, better, operating systems out there. Like\n");
        printf("         Mac, DOS, or Amiga-OS. Or, better yet even, would be another free Unix\n");
        printf("         OS like FreeBSD.\n");
        printf("8 L--    Unix sucks. Because Linux = Unix, Linux sucks. I worship Bill Gates.\n");
        printf("9 L---   I am Bill Gates.\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your GNU/Linux code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 9);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_emacs(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Emacs                                                             Page %i of %i\n", PAGES-(PAGES-10), PAGES);
	printf("===============================================================================\n");
        printf("1 E+++  Emacs is my login shell! M-x doctor is my psychologist! I use Emacs to\n");
        printf("        control my TV and toaster oven! All you vi people don't know what you\n");
        printf("        are missing! I read alt.religion.emacs, alt.sex.emacs and comp.os.emacs\n");
        printf("2 E++   I know and use elisp regularly!\n");
        printf("3 E+    Emacs is great! I read my mail and news with it!\n");
        printf("4 E     Yeah, I know what Emacs is, and use it as my regular editor.\n");
        printf("5 E-    Emacs is too big and bloated for my tastes.\n");
        printf("6 E--   Emacs is just a fancy word processor.\n");
        printf("7 E---  Emacs sucks! vi forever!\n");
        printf("8 E---- Emacs sucks! pico forever!\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Emacs code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 8);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_www(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("World Wide Web                                                    Page %i of %i\n", PAGES-(PAGES-11), PAGES);
	printf("===============================================================================\n");
        printf("1 W+++ I am a WebMaster. Don't even think about trying to view my page without\n");
        printf("       the latest version of Netscape. When I'm not on my normal net connection\n");
        printf("       I surf the web using my Newton and a cellular modem.\n");
        printf("2 W++  I have a homepage. I surf daily. My homepage is advertised in my .sig\n");
        printf("3 W+   I have the latest version of Netscape, and wander the web only when\n");
        printf("       there is something specific I'm looking for.\n");
        printf("4 W    I have a browser and a connection. Occasionally I'll use them.\n");
        printf("5 W-   The web is really a pain. Life was so much easier when you could\n");
        printf("       transfer information by simple ASCII. Now everyone won't even consider\n");
        printf("       your ideas unless you spiff them up with bandwidth-consuming pictures\n");
        printf("       and pointless information links.\n");
        printf("6 W--  A pox on the web! It wastes time and bandwidth and just gives the\n");
        printf("       uneducated morons a reason to clutter the Internet.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("Enter your World Wide Web code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 6);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_usenet(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("USENET News                                                       Page %i of %i\n", PAGES-(PAGES-12), PAGES);
	printf("===============================================================================\n");
        printf(" 1 N++++ I am Tim Pierce.\n");
        printf(" 2 N+++  I read so many newsgroups that the next batch of news comes in before\n");
        printf("         I finish reading the last batch, and I have to read for about 2 hours\n");
        printf("         straight before I'm caught up on the morning's news. Then there's the\n");
        printf("         afternoon...\n");
        printf(" 3 N++   I read all the news in a select handful of groups.\n");
        printf(" 4 N+    I read news recreationally when I have some time to kill.\n");
        printf(" 5 N     Usenet News? Sure, I read that once.\n");
        printf(" 6 N-    News is a waste of my time and I avoid it completely.\n");
        printf(" 7 N--   News sucks! 'Nuff said.\n");
        printf(" 8 N---  I work for Time magazine.\n");
        printf(" 9 N---- I am a Scientologist.\n");
        printf("10 N*    All I do is read news.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your USENET News code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 10);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_oracle(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("USENET Oracle                                                     Page %i of %i\n", PAGES-(PAGES-13), PAGES);
	printf("===============================================================================\n");
        printf("1 o+++++ I am Steve Kinzler.\n");
        printf("2 o++++  I am an active Priest.\n");
        printf("3 o+++   I was a Priest, but have retired.\n");
        printf("4 o++    I have made the Best of Oracularities.\n");
        printf("5 o+     I have been incarnated at least once.\n");
        printf("6 o      I've submitted a question, but it has never been incarnated.\n");
        printf("7 o-     I sent my question to the wrong group and I got flamed.\n");
        printf("8 o--    Who needs answers from a bunch of geeks anyhow?\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your USENET Oracle code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 8);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_kibo(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Kibo                                                              Page %i of %i\n", PAGES-(PAGES-14), PAGES);
	printf("===============================================================================\n");
        printf(" 1 K++++++ I am Kibo.\n");
        printf(" 2 K+++++  I've had sex with Kibo.\n");
        printf(" 3 K++++   I've met Kibo.\n");
        printf(" 4 K+++    I've gotten mail from Kibo.\n");
        printf(" 5 K++     I've read Kibo.\n");
        printf(" 6 K+      I like Kibo.\n");
        printf(" 7 K       I know who Kibo is.\n");
        printf(" 8 K-      I don't know who Kibo is.\n");
        printf(" 9 K--     I dislike Kibo.\n");
        printf("10 K---    I am currently hunting Kibo down with the intent of ripping his\n");
        printf("           still-beating heart of out his chest and showing it to him as\n");
        printf("           he dies.\n");
        printf("11 K----   I am Xibo.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Kibo code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 11);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_windows(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Microsoft Windows                                                 Page %i of %i\n", PAGES-(PAGES-15), PAGES);
	printf("===============================================================================\n");
        printf("1 w+++++ I am Bill Gates.\n");
        printf("2 w++++  I have Windows, Windows 95, Windows NT, and Windows MT Advanced Server\n");
        printf("         all running on my SMP RISC machine. I haven't seen daylight in months.\n");
        printf("3 w+++   I am a MS Windows programming god. I wrote a VxD driver to allow\n");
        printf("         MS Windows and DOS to share the use of my waffle iron. P.S. Unix sux.\n");
        printf("4 w++    I write MS Windows programs in C and think about using C++ someday.\n");
        printf("         I've written at least one DLL.\n");
        printf("5 w+     I have installed my own custom sounds, wallpaper, and screen savers so\n");
        printf("         my PC walks and talks like a fun house. Oh yeah, I have a hundred\n");
        printf("         TrueType(tm) fonts that I've installed but never used. I never lose at\n");
        printf("         Minesweeper or Solitaire.\n");
        printf("6 w      OK, so I use MS Windows, I don't have to like it.\n");
        printf("7 w-     I'm still trying to install MS Windows and have at least one\n");
        printf("         peripheral that never works right.\n");
        printf("8 w--    MS Windows is a joke operating system. Hell, it's not even an\n");
        printf("         operating system. NT is Not Tough enough for me either. 95 is how many\n");
        printf("         times it will crash an hour.\n");
        printf("9 w---   Windows has set back the computing industry by at least 10 years. Bill\n");
        printf("         Gates should be drawn, quartered, hung, shot, poisoned, disembowelled,\n");
        printf("         and then REALLY hurt.\n");
        printf("\n");
	printf("Enter your Microsoft Windows code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 9);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_os2(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("OS/2                                                              Page %i of %i\n", PAGES-(PAGES-16), PAGES);
	printf("===============================================================================\n");
        printf("1 O+++  I live, eat and breathe OS/2. All of my hard drives are HPFS.\n");
        printf("        I am the Anti-Gates.\n");
        printf("2 O++   I use OS/2 for all my computing needs. I use some DOS and Windows\n");
        printf("        programs, but run them under OS/2. If the program won't run under Os/2,\n");
        printf("        then obviously I don't need it.\n");
        printf("3 O+    I keep a DOS partition on my hard drive 'just in case'. I'm afraid to\n");
        printf("        try HPFS.\n");
        printf("4 O     I finally managed to get OS/2 installed but wasn't terribly impressed.\n");
        printf("5 O-    Tried it, didn't like it.\n");
        printf("6 O--   I can't even get the thing to install!\n");
        printf("7 O---  Windows RULES! Long live Bill Gates. (See w++++)\n");
        printf("8 O---- I am Bill Gates of Borg. Os/2 is irrelevant.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your OS/2 code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 8);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_mac(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Macintosh                                                         Page %i of %i\n", PAGES-(PAGES-17), PAGES);
	printf("===============================================================================\n");
        printf("1 M++ I am a Mac guru. Anything those DOS putzes and Unix nerds can do, I can\n");
        printf("      do better, and if not, I'll write the damn software to do it.\n");
        printf("2 M+  A Mac has it's uses and I use it quite often.\n");
        printf("3 M   I use a Mac, but I'm pretty indifferent about it.\n");
        printf("4 M-  Macs suck. All real geeks have a character prompt.\n");
        printf("5 M-- Macs do more than suck. They make a user stupid by allowing them to use\n");
        printf("      the system without knowing what they are doing. Mac weenies have lower\n");
        printf("      IQs than the fuzz in my navel.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Macintosh code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 5);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_vms(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("VMS                                                               Page %i of %i\n", PAGES-(PAGES-18), PAGES);
	printf("===============================================================================\n");
        printf("1 V+++ I am a VMS sysadmin. I wield far more power than those Unix admins,\n");
        printf("       because Unix can be found on any dweeb's desktop. Power through\n");
        printf("       obscurity is my motto.\n");
        printf("2 V++  Unix is a passing fad compared to the real power in the universe,\n");
        printf("       my VMS system.\n");
        printf("3 V+   I tend to like VMS better than Unix.\n");
        printf("4 V    I've used VMS.\n");
        printf("5 V-   Unix is much better than VMS for my computing needs.\n");
        printf("6 V--  I would rather smash my head repeatedly into a brick wall than suffer\n");
        printf("       the agony of working with VMS. It's reminiscent of a dead and decaying\n");
        printf("       pile of moose droppings. Unix rules the universe.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your VMS code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 6);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

