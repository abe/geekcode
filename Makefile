CFLAGS=-O3 -s
TARGET=geekcode
CC=gcc

SRCFILES = gc_appearance.c    \
           gc_computers.c     \
           gc_entertainment.c \
           gc_lifestyle.c     \
           gc_politics.c      \
           gc_type.c          \
           gc_consoleio.c     \
           gc_translate.c     \
           geekcode.c

LIBFILES = gc_appearance.o    \
           gc_computers.o     \
           gc_entertainment.o \
           gc_lifestyle.o     \
           gc_politics.o      \
           gc_type.o          \
           gc_consoleio.o     \
           gc_translate.o     \
           geekcode.o

all: ${TARGET}

clean:
	rm -f ${TARGET} ${LIBFILES}

geekcode:
	${CC} ${CFLAGS} -c gc_appearance.c
	${CC} ${CFLAGS} -c gc_computers.c
	${CC} ${CFLAGS} -c gc_entertainment.c
	${CC} ${CFLAGS} -c gc_lifestyle.c
	${CC} ${CFLAGS} -c gc_politics.c
	${CC} ${CFLAGS} -c gc_type.c
	${CC} ${CFLAGS} -c gc_consoleio.c
	${CC} ${CFLAGS} -c gc_translate.c
	${CC} ${CFLAGS} -c geekcode.c
	${CC} ${CFLAGS} -o ${TARGET} ${LIBFILES}

install:
	install -m 755 geekcode /usr/local/bin
