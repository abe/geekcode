/*
 gc_lifestyle.c - Functions for the Lifestyle section

 Geek Code Generator v1.7.3 - Generates your geek code
 Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdio.h>
#include "geekcode.h"

int get_education(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Education                                                         Page %i of %i\n", PAGES-(PAGES-32), PAGES);
	printf("===============================================================================\n");
        printf("1 e+++++ I am Stephen Hawking\n");
        printf("2 e++++  Managed to get my Ph.D.\n");
        printf("3 e+++   Got a Master degree\n");
        printf("4 e++    Got a Bachelors degree\n");
        printf("5 e+     Got an Associates degree\n");
        printf("6 e      Finished High School\n");
        printf("7 e-     Haven't finished High School\n");
        printf("8 e--    Haven't even entered High School\n");
        printf("9 e*     I learned everything there is to know about life from the\n");
        printf("         \"Hitchhiker's Trilogy\"\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Education code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 9);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_housing(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Housing                                                           Page %i of %i\n", PAGES-(PAGES-33), PAGES);
	printf("===============================================================================\n");
        printf("1 h++   Living in a cave with 47 computers and an Internet feed, located near a\n");
        printf("        Dominoes pizza (See !d)\n");
        printf("2 h+    Living alone, get out once a week to buy food, no more than once a\n");
        printf("        month to do laundry. All surfaces covered.\n");
        printf("3 h     Friends come over to visit every once in a while to talk about Geek\n");
        printf("        things. There is a place for them to sit.\n");
        printf("4 h-    Living with one or more registered Geeks.\n");
        printf("5 h--   Living with one or more people who know nothing about being a Geek and\n");
        printf("        refuse to watch Babylon 5.\n");
        printf("6 h---  Married, (persons living romantically with someone might as well label\n");
        printf("        themselves h---, you're as good as there)\n");
        printf("7 h---- Married with children - Al Bundy can sympathize.\n");
        printf("8 h!    I am stuck living with my parents!\n");
        printf("9 h*    I'm not sure where I live anymore. This lab/workplace seems like home\n");
        printf("        to me.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Housing code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 9);

    if (selection == 0)
	    exit(1);
    else
	    return selection;
}

int get_relationships(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Relationships                                                     Page %i of %i\n", PAGES-(PAGES-34), PAGES);
	printf("===============================================================================\n");
        printf(" 1 r+++ Found someone, dated, and am now married.\n");
        printf(" 2 r++  I've dated my current S.O. for a long time.\n");
        printf(" 3 r+   I date frequently, bouncing from one relationship to another.\n");
        printf(" 4 r    I date periodically.\n");
        printf(" 5 r-   I have difficulty maintaining a relationship.\n");
        printf(" 6 r--  People just aren't interested in dating me.\n");
        printf(" 7 r--- I'm beginning to think that I'm a leper or something, the way people\n");
        printf("        avoid me like the plague.\n");
        printf(" 8 !r   I've never had a relationship.\n");
        printf(" 9 r*   Signifying membership in the SBCA (Sour Bachelor(ette)'s Club of\n");
        printf("        America). The motto is 'Bitter, but not Desperate'.\n");
        printf("        First founded at Caltech.\n");
        printf("10 r%%   I was going out with someone, but the asshole dumped me.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Relationships code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 10);

    if (selection == 0)
	    exit(1);
    else
	    return selection;
}

int get_sex_type(void)
{
   int selection = 99;

   do
   {
      clearscreen();
      printf("Sex Type                                                         Page %ia of %i\n", PAGES-(PAGES-35), PAGES);
      printf("===============================================================================\n");
      printf("1 x - I am a female\n");
      printf("2 y - I am a male\n");
      printf("3 z - I do not wish to disclose my gender\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("\n");
      printf("Enter your Sex type code number here [0 to quit]: ");
      scanf("%d", &selection);
      clear_kb();
   } while (selection < 0 || selection > 3);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

char show_sex_type(int x)
{
   char *sex;

   switch (x)
   {
    case  1: sex="x"; break;
    case  2: sex="y"; break;
    case  3: sex="z"; break;
    default: sex="z"; break;
   }

   return *sex;
}

int get_sex(int x)
{
   int selection = 99;
   char *sex;

   switch (x)
   {
    case  1: sex="x"; break;
    case  2: sex="y"; break;
    case  3: sex="z"; break;
    default: sex="z"; break;
   }

   do
     {
	clearscreen();
 	printf("Sex                                                              Page %ib of %i\n", PAGES-(PAGES-35), PAGES);
 	printf("===============================================================================\n");
        printf(" 1 %s+++++ I am Madonna\n", sex);
        printf(" 2 %s++++  I have a few little rug rats to prove I've been there. Besides, with\n", sex);
        printf("          kids around, who has time for sex?\n");
        printf(" 3 %s+++   I'm married, so I can get it (theoretically) whenever I want.\n", sex);
        printf(" 4 %s++    I was once referred to as 'easy'. I have no idea where that might\n", sex);
        printf("          have come from though.\n");
        printf(" 5 %s+     I've had real, live sex.\n", sex);
        printf(" 6 %s      I've had sex. Oh! You mean with someone else? Then no.\n", sex);
        printf(" 7 %s-     Not having sex by choice.\n", sex);
        printf(" 8 %s--    Not having sex because I just can't get any...\n", sex);
        printf(" 9 %s---   Not having sex because I'm a nun or priest.\n", sex);
        printf("10 %s*     I'm a pervert.\n", sex);
        printf("11 %s**    I've been known to make perverts look like angels.\n", sex);
        printf("12 !%s     Sex? What's that? I've had no sexual experiences.\n", sex);
        printf("13 %s?     It's none of your business what my sex life is like (this is used to\n", sex);
        printf("          denote your gender only).\n");
        printf("14 !%s+    Sex? What's that? No experience, willing to learn!\n", sex);
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Sex code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 14);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

